-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 28, 2019 at 06:56 AM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.21-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lv_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Alyce Kertzmann', 'cyrus31@example.com', '2019-08-27 23:18:10', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'sEVbgyIAmg', '2019-08-27 23:18:11', '2019-08-27 23:18:11'),
(2, 'Prof. Alaina Thompson MD', 'jones.maya@example.org', '2019-08-27 23:18:10', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'SUzFZugVyO', '2019-08-27 23:18:11', '2019-08-27 23:18:11'),
(3, 'Jerod Schmitt', 'stanton.clarissa@example.org', '2019-08-27 23:18:10', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'kvvqBColgZ', '2019-08-27 23:18:11', '2019-08-27 23:18:11'),
(4, 'Trey McDermott', 'alisha81@example.com', '2019-08-27 23:18:10', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'N3v0nj8z5T', '2019-08-27 23:18:11', '2019-08-27 23:18:11'),
(5, 'Terry Cummings', 'shemar.wunsch@example.net', '2019-08-27 23:18:10', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'VFZYf1YgWY', '2019-08-27 23:18:11', '2019-08-27 23:18:11'),
(6, 'Arlo Ferry', 'britney71@example.org', '2019-08-27 23:18:10', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '93ZusM18gj', '2019-08-27 23:18:11', '2019-08-27 23:18:11'),
(7, 'Jada Volkman', 'maxine.buckridge@example.org', '2019-08-27 23:18:10', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '38y2s4eF4q', '2019-08-27 23:18:11', '2019-08-27 23:18:11'),
(8, 'Mariane Luettgen', 'jamil.ohara@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'TGOOkW4dKE', '2019-08-27 23:18:11', '2019-08-27 23:18:11'),
(9, 'Stevie Farrell', 'branson79@example.com', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '9LtjtYx1hP', '2019-08-27 23:18:11', '2019-08-27 23:18:11'),
(10, 'Mrs. Fleta Pfeffer', 'conroy.nettie@example.net', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Be9iJdR4v8', '2019-08-27 23:18:11', '2019-08-27 23:18:11'),
(11, 'Delia Cartwright', 'plebsack@example.com', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'xEIWavbqsg', '2019-08-27 23:18:11', '2019-08-27 23:18:11'),
(12, 'Prof. Ryan Torphy PhD', 'yhoeger@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'qSExhU7fCj', '2019-08-27 23:18:11', '2019-08-27 23:18:11'),
(13, 'Estefania Heathcote V', 'ukautzer@example.com', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'r6M5oCfo4I', '2019-08-27 23:18:11', '2019-08-27 23:18:11'),
(14, 'Tristin Vandervort', 'abdiel.hoppe@example.net', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'fmlzg2UR9g', '2019-08-27 23:18:11', '2019-08-27 23:18:11'),
(15, 'Prof. Connie Langosh', 'krystal.vandervort@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'StdCleiuHY', '2019-08-27 23:18:11', '2019-08-27 23:18:11'),
(16, 'Prof. Santiago Rippin DDS', 'noe21@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'yjdOkNDfuW', '2019-08-27 23:18:11', '2019-08-27 23:18:11'),
(17, 'Anibal Kulas', 'padberg.thalia@example.com', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'RAfXors37H', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(18, 'Mandy Heathcote', 'kgraham@example.com', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'VEy39ib6vx', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(19, 'Mr. Troy Ryan III', 'domenica38@example.net', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'bOYaabKgTm', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(20, 'Jammie Carter DVM', 'bayer.itzel@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'oJFJHdsFLW', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(21, 'Shea Daugherty', 'mertie.oreilly@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'YA9igxpIzZ', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(22, 'Ena Keeling', 'tyshawn85@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'fGg6AQPEcM', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(23, 'Allison Wiegand', 'kassandra.bruen@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'enm8xwHKpz', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(24, 'Conrad Howe', 'alberto38@example.net', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'YPblbunxeG', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(25, 'Lorenzo Franecki', 'abby35@example.com', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Ez305R3L3t', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(26, 'Prof. Wanda Ward Sr.', 'immanuel.hegmann@example.com', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'MeJ5KspqAh', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(27, 'Ida Casper Jr.', 'brayan.tillman@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'EBpxTHn7dz', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(28, 'Filiberto Terry', 'krowe@example.com', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '3dcpEM6q2C', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(29, 'Marcelle Padberg', 'benton71@example.com', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '49ZZVin96y', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(30, 'Kailee Cummerata', 'waylon.torp@example.com', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '58AvYmZwAQ', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(31, 'Brayan Hettinger', 'ava47@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'dexhi2m3C8', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(32, 'Lila Larkin', 'jennings41@example.com', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'DHMeHxdmjE', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(33, 'Leonora Farrell', 'donavon.wyman@example.net', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ohpB9NVXMz', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(34, 'Raphael Hartmann', 'yruecker@example.net', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'LYktPhRt1Z', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(35, 'Prof. Bernard Beer Sr.', 'schuster.erna@example.net', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'mR4HoZpMP0', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(36, 'Mr. Conrad Cartwright', 'julian.gerlach@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'tjOns4yFc9', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(37, 'Chaz Runolfsdottir', 'quigley.malika@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'c3fRNTJub6', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(38, 'Lavinia Leannon', 'friesen.eunice@example.com', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'HxrVdyAAoA', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(39, 'Rachael Davis', 'filomena00@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ZYZ44PADAj', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(40, 'Miss Laura Rodriguez Sr.', 'deckow.lavern@example.net', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Xmogdn3QCT', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(41, 'Sabina Buckridge', 'harris.jalyn@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'RLyp8GT47i', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(42, 'Ignatius Rutherford Jr.', 'verona29@example.com', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'v97gpkRYGF', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(43, 'Prof. Sven Corkery', 'lura86@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ta2Blc4T3p', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(44, 'Mrs. Joanie Lind', 'trystan.boyer@example.com', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ZMkJu5WCqb', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(45, 'Dr. Charles Wisoky Jr.', 'yryan@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'cXIVgLkIbT', '2019-08-27 23:18:12', '2019-08-27 23:18:12'),
(46, 'Morgan Grimes', 'aschamberger@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2iSxyyZOF0', '2019-08-27 23:18:13', '2019-08-27 23:18:13'),
(47, 'Craig Lind', 'montana51@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '3hsbehUwJr', '2019-08-27 23:18:13', '2019-08-27 23:18:13'),
(48, 'Eli Weimann MD', 'leone85@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 't20jjfSRfT', '2019-08-27 23:18:13', '2019-08-27 23:18:13'),
(49, 'Mr. Darian Lockman', 'benjamin56@example.net', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '6mNzkWqwms', '2019-08-27 23:18:13', '2019-08-27 23:18:13'),
(50, 'Zella Maggio', 'kennith.champlin@example.org', '2019-08-27 23:18:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'TN1ZnwP2bn', '2019-08-27 23:18:13', '2019-08-27 23:18:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
